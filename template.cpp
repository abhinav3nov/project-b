#include<iostream>
#include<iomanip>

using namespace std;

template<typename T>
 T pi = T(3.1415926735897932385L);
template<typename T>
T area_of_circle(const T &r){
    return r*r*pi<T>;
}
int main(){
    cout.precision(20);
    
    cout<<"double"<< pi<double> <<endl;
   
    auto a=area_of_circle< double >(3.6547);
    cout<<"area of circle:"<< a<<endl;
    return 0;
}