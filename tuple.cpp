#include<iostream>
#include<tuple>
#include<string>
using namespace std;
template <typename T>
void printpair( T t){
    auto tupleSize=tuple_size<decltype(t)>::value;
    if (tupleSize != 3) 
      return;
    cout<<get<0>(t)<<":"<<get<1>(t)<<":"<<get<2>(t)<<endl;
}
int main(){
    //initialiser list
    tuple <int,string,int> p1={1,"abinav",2434};
    printpair (p1);

    //default contructor
    tuple<int,string,int > p2(2,"anshika",42432);
    printpair (p2);

     //default contructor
    tuple <int,string,int> p3 = make_tuple(3,"adfcdsva",31314);
    printpair (p3);

}
