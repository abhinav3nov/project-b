#include<iostream>
#include <vector>
using namespace std;
int main(){
    cout<<"Vector from Initializer list :"<<endl;
    vector <int> vil ={1,2,3,4,5,6,7,8,9,10};

    cout<< "size: "<< vil.size()<<endl;
    cout<<"front:  " <<vil.front() <<endl;
    cout<<"back:  " <<vil.back() <<endl;

    cout<< endl << "Iterator:" <<endl;
    auto itbegin =vil.begin();
    auto itend =vil.end ();
    for (auto it = itbegin;it !=itend ;it++){
        cout <<*it<<" ";

    }
    cout <<endl;
    for (auto i=0;i<vil.size();i++)
      cout<<vil.at(i)<<" ";
cout<<endl;
 for (auto & i: vil)
      cout<<i<<" ";
cout<<endl<<"Insert at begening +5"<<endl;
vil.insert (vil.begin()+5,42);
cout<<"size" <<vil.size()<<endl;
cout<<"vil[5]:"<<vil[5]<<endl;   

for (auto & i: vil)
      cout<<i<<" ";

cout<<endl<<"eraser at begening +5"<<endl;
vil.erase (vil.begin()+5);
cout<<"size" <<vil.size()<<endl;
cout<<"vil[5]:"<<vil[5]<<endl;   

for (auto & i: vil)
      cout<<i<<" ";

cout<<endl<<"pushback at begening"<<endl;
vil.push_back (47);
cout<<"size" <<vil.size()<<endl;
cout<<"vil.back()"<<vil.back()<<endl;   

for (auto & i: vil)
      cout<<i<<" ";



}