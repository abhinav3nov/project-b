#include<iostream>
#include<utility>
#include<string>
using namespace std;
template <typename T1,typename T2>
void printpair( pair<T1,T2> & p){
    cout<<p.first<<":"<<p.second<<endl;
}
int main(){
    //initialiser list
    pair <int,string> p1={1,"abinav"};
    printpair (p1);

    //default contructor
    pair <int,string> p2(2,"anshika");
    printpair (p2);

     //default contructor
    pair <int,string> p3 = make_pair(3,"adfcdsva");
    printpair (p3);

}
