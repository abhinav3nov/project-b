#include<iostream>
#include<typeinfo>
#include<string>
using namespace std;
template <typename tl,typename tr>
auto tf(const tl &lhs, tr &rhs){
    cout<<typeid(rhs).name();
    auto z = lhs+rhs;
    return z;

}
int main(){
    int i =47;
     char cstr[] = "this is a c-string ";
    const string sc = "this is string class string";

auto x="this is a c-string";
decltype (x) y;//decltype is way to declare type ,it derive the type from expression
cout << "type i:"<< typeid(i).name()<<endl;
cout << "type cstr:"<< typeid(cstr).name()<<endl;
cout << "type string class:"<< typeid(sc).name()<<endl;
cout << "type x:"<< typeid(x).name()<<endl;

for (auto it =sc.begin();it != sc.end();it ++)
{
    cout << *it <<" ";
}
cout <<"\n" ;
for (auto ii:sc)
{  ii='c';
    cout << ii <<" ";
}
for (auto it =sc.begin();it != sc.end();it ++)
{
    cout << *it <<" ";
}
cout <<"\n" ;
auto  z = tf<string,char *&>(sc,cstr);
cout<<"z is "<<z<<endl;
cout<<"type of z is "<<typeid(z).name()<<"\n";

}
