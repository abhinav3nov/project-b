#include<iostream>
#include<exception>
using namespace std;
class E :public exception {

    const char *msg;
    E();
    public:
    E(const char *s ) :msg(s){}
    const char *what() const throw() {return msg;}
};
const E e_ouch("outch");
const E e_worse("dont do that");
void broken(){
    cout <<"this is a broken fn\n";
    throw e_worse;

}
int main(){
    cout<< "let's hav a emergency "<<endl;
    try{
    broken();
    }catch (exception &e){
        cout<<e.what()<<endl;
    }

    return 0;
}