#include<iostream>
#include <exception>

using namespace std;
class E: public exception{
    const char *msg;
    E(){};
    public:
    explicit E(const char *s) throw() :msg (s) {}
    const char *what () const throw () {return msg;}  
};
template <typename T>
class Stack{
    private:
    static const int defaultSize=5;
    static const int maxsize=1000;
    int _size;
    int _top;
    T *_stkptr;

    public:
     Stack (int s=defaultSize){
         if (s<1 || s>maxsize) throw E("invalid stack size");
         else 
         _size=s;

         _stkptr= new T [_size];
         _top=-1;
     }
    bool isfull() const {
        return _top>=_size -1;

    }
    bool isempty() const {
        return _top <0;
    }
    T & push (const T & data){
        if (isfull()) throw E("Stack is Full");
        return _stkptr[++_top]=data;

    }
    T &pop (){
        if (isempty()) throw E("Stack is Empty");
        return _stkptr[_top--];
    }
    int size() const {
        return _size;
    }
     int top() const { 
         return _top; 
    }


};
int main(){
        try { 
         Stack<string> si(5);
         cout << "si size:"<<si.size()<< endl;
         cout << "si top: "<< si.top()<<endl; 
         for (auto i: {"abhinav","sriavsava","anshika"})
            si.push(i);
                 
          cout << "si is :"<< ( si.isfull() ? "" : "not " ) << "full" << endl;
         cout << "si top after pushes: "<< si.top()<<endl;
         while (!si.isempty()){
             cout << "pooped:    " <<si.pop()<<endl;
         }
         }  catch (E & e){
             cout << "Stack error:" <<e.what() <<endl;
         }
          try { 
         Stack<float> si(5);
         cout << "si size:"<<si.size()<< endl;
         cout << "si top: "<< si.top()<<endl; 
         for (auto i: {1.23,3.4,.09924})
            si.push(i);
                 
          cout << "si is :"<< ( si.isfull() ? "" : "not " ) << "full" << endl;
         cout << "si top after pushes: "<< si.top()<<endl;
         while (!si.isempty()){
             cout << "pooped:    " <<si.pop()<<endl;
         }
         }  catch (E & e){
             cout << "Stack error:" <<e.what() <<endl;
         }


}
